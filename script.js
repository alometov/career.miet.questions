var root = document.querySelector('#root')

var questions = [
    "Что действительно тебя мотивирует двигаться к цели?",
    "Что в себе важно менять, чтобы добиться желаемого результата?",
    "С чего ты готов(а) начать?",
    "Что в желаемом результате зависит только от тебя?",
    "Какие ключевые шаги ты видишь на пути к цели?",
    "О каких препятствиях тебе стоит подумать?",
    "Что для тебя самое интересное в достижении цели?",
    "Чему ты хочешь уделять больше всего времени?",
    "Что больше всего тебя вдохновляет в этой цели?",
    "Что для тебя ценного в достижении этой цели?",
    "Какая самая большая преграда на пути к цели? Какие есть возможности её преодолеть?",
    "Какие есть возможности, чтобы двигаться к цели новыми способами?",
    "Как ты поймешь, что ты уже на пути к цели?",
    "Чего на самом деле ты хочешь?",
    "Что должно поменяться в твоем мышлении, чтобы достичь результата?",
    "Какова твоя роль в этом деле?",
    "Какие возможности дает тебе наличие желаемого результата?",
    "Какие свои сильные стороны ты будешь использовать для достижения цели?",
    "Какое ты видишь наилучшее решение?",
    "Кто может тебе помочь в достижении цели?",
    "Во что тебе нужно поверить, чтобы был успех?",
    "Каковы ключевые результаты достижения цели?",
    "Что ты поменяешь в завтрашнем дне, чтобы в срок получить результат?",
    "Какое напутствие ты себе хочешь дать прежде чем начать движение к своей цели?",
    "К кому ты мог(ла) бы обратиться за советом?",
    "По каким признакам ты поймешь, что достиг желаемого результата?",
    "Чей пример тебя может вдохновить на достижение наилучшего результата?",
    "В чём твоя суперсила?",
    "Каковы будут последствия для тебя и для других?",
]

function makeLink() {
    var $link = document.createElement('a')
    $link.href = 'https://vk.com/career.miet'
    $link.innerText = 'career.miet'
    $link.classList.add('btn', 'question__link')

    $arrow = document.createElement('img')
    $arrow.src = '/assets/arrow.svg'

    $link.appendChild($arrow)

    return $link
}

function makeNextButton(nextIdx) {
    var $nextButton = document.createElement('button')
    $nextButton.innerText = 'Следующий вопрос'
    $nextButton.classList.add('btn', 'question__next')
    $nextButton.addEventListener('click', () => {
        $currentQuestion = document.querySelector('.active')
        $nextQuestion = $currentQuestion.nextSibling

        if ($nextQuestion) {
            $currentQuestion.classList.remove('active')
            $nextQuestion.classList.add('active')
        } else {
            $currentQuestion.classList.remove('active')
            $finish = makeFinish()
            root.appendChild($finish)
        }
    })

    return $nextButton
}

function makeQuestions() {
    questions.forEach((question, index) => {
        var $section = document.createElement('section')
        $section.classList.add('question')
        index == 0 && $section.classList.add('active')

        var $link = makeLink()
        var $question = document.createElement('p')
        $question.innerText = question
        $question.classList.add('question__text')
        var $nextBtn = makeNextButton(index)

        $section.appendChild($link)
        $section.appendChild($question)
        $section.appendChild($nextBtn)

        root.appendChild($section)
    })
}

function makeFinish() {
    var $section = document.createElement('section')
    $section.classList.add('finish')

    var $title = document.createElement('strong')
    $title.innerText = 'Вопросы закончились!'

    var $text = document.createElement('p')
    $text.innerText = 'Надеемся наши вопросы помогли тебе стать ближе к своей цели'

    var $linkBar = makeLinkBar()

    $section.appendChild($title)
    $section.appendChild($text)
    $section.appendChild($linkBar)

    return $section
}

function makeLinkBar() {
    var $linkBar = document.createElement('div')
    $linkBar.classList.add('link-bar')

    var $resetBtn = document.createElement('button')
    $resetBtn.innerText = 'Начать заново'
    $resetBtn.classList.add('btn')
    $resetBtn.addEventListener('click', () => {
        root.innerHTML = ''
        makeQuestions()
    })

    var $vkLink = document.createElement('a')
    $vkLink.href = 'https://vk.com/career.miet'
    $vkIcon = document.createElement('img')
    $vkIcon.src = '/assets/vk.svg'
    $vkLink.appendChild($vkIcon)

    var $tgLink = document.createElement('a')
    $tgLink.href = 'https://t.me/careermiet'
    $tgIcon = document.createElement('img')
    $tgIcon.src = '/assets/tg.svg'
    $tgLink.appendChild($tgIcon)

    $linkBar.appendChild($resetBtn)
    $linkBar.appendChild($vkLink)
    $linkBar.appendChild($tgLink)

    return $linkBar
}

window.onload = () => {
    makeQuestions()
}